
#include <RemoteSwitch.h>

/* 
 * Control remote mains switches via 433,92 MHz AM TX module
 * connected to pin 10
 *
 * 2017-06-27 Alex The Engineer, Rat Salad Park, IPSWICH GB
 * 
 * uses the RCSwitch library
 * 
 * 2017-06-28 Now recoded to use Remoteswitch library
 * 
 */



// the LED output pins

const int pinLED01=13;
const int pinLED02=12;
const int pinLED03=8;
const int pinLED04=7;
const int pinLEDin=2;

// to blink the indicator LED
// on 02

int ledState = LOW;
unsigned long previousMillis = 0;
unsigned long interval = 375;

Ener002Switch enerswitch(10);
// HomeEasyEUSwitch euroswitch(10);
BlokkerSwitch2 blok2switch(10);


// this is 10110101011000000011 (1st 20 bits) in decimal
const unsigned long euro=0xb5603;


void setup() {
  

 // put off the other two older type which are Blokker2 protocol
 blok2switch.sendSignal('A',1,false);
 blok2switch.sendSignal('A',2,false);
 //put off all the ASDA switches, true and false are reversed here
 enerswitch.sendSignal(euro,7,true); 
 // put off the indicator LEDs
  pinMode(pinLED01,OUTPUT);
  pinMode(pinLED02,OUTPUT);
  pinMode(pinLED03,OUTPUT);
  pinMode(pinLED04,OUTPUT);
  pinMode(pinLEDin,OUTPUT);
  digitalWrite(pinLED01,LOW);
  digitalWrite(pinLED02,LOW);
  digitalWrite(pinLED03,LOW);
  digitalWrite(pinLED04,LOW);
  digitalWrite(pinLEDin,LOW);
  Serial.begin(9600); // open the tty
}

void loop() {

  if (Serial.available() > 0) {
    int inByte = Serial.read();

    // do something different depending on the character received.
    // The switch statement expects single number values for each case;
    // in this exmaple, though, you're using single quotes to tell
    // the controller to get the ASCII value for the character.  For
    // example 'a' = 97, 'b' = 98, and so forth:

    switch (inByte) {
      case 'a': // 01 off
        enerswitch.sendSignal(euro,1,false);
        digitalWrite(pinLED01,LOW);
        interval=200;
        break;
      case 'b': // 01 on
        enerswitch.sendSignal(euro,1,true);
        digitalWrite(pinLED01,HIGH);
        interval=500 ; 
        break;
      case 'c': // 02 off
        enerswitch.sendSignal(euro,2,false);
        digitalWrite(pinLED02,LOW);
        interval=100;
        break;
      case 'd': // 02 on 
        enerswitch.sendSignal(euro,2,true);
        digitalWrite(pinLED02,HIGH);
        interval=700;
        break;
      case 'e': // blokker A1 off
        blok2switch.sendSignal('A',1,false);
        digitalWrite(pinLED03,LOW);
        interval=600;
        break;
      case 'f': // blokker A1 on
        blok2switch.sendSignal('A',1,true);
        digitalWrite(pinLED03,HIGH);
        interval=700;
        break;  
      case 'g': // blokker A2 off
        blok2switch.sendSignal('A',2,false);
        digitalWrite(pinLED04,LOW);
        interval=100;
        break;
      case 'h': // blokker A2 on
        blok2switch.sendSignal('A',2,true);
        digitalWrite(pinLED04,HIGH);
        interval=900;
        break;  
        
      case 'y': // all OFF
        enerswitch.sendSignal(euro,7,true);
        digitalWrite(pinLED01,LOW);
        digitalWrite(pinLED02,LOW);
      //  digitalWrite(pinLED03,LOW);
      //  digitalWrite(pinLED04,LOW);
        interval = 375;
        break;
      case 'z': // all ON
        enerswitch.sendSignal(euro,7,false);
        digitalWrite(pinLED01,HIGH);
        digitalWrite(pinLED02,HIGH);
      //  digitalWrite(pinLED03,HIGH);
      //  digitalWrite(pinLED04,HIGH);
        interval = 900;
        break;

//      default:
      }         

  }

// if there isn't any incoming tty data to deal with
// now blink the status LED on 02 with 
// whatever interval is currently set

  unsigned long currentMillis = millis();
  
  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
      } 
    else {
      ledState = LOW;
      }
  // set the LED with the ledState of the variable:
    digitalWrite(pinLEDin, ledState);
  }
  
  
}

